<?php
ini_set('max_execution_time', 900);
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */
global $template;
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
$args = [
    'product' => $post,
];
$product_category_ids = wc_get_product_term_ids($post->ID, 'product_cat');
$product_category_slugs = wc_theme_get_slug_by_term_id($product_category_ids);

get_header( 'woocommerce' );
?>
<script type="text/javascript">
    $(document).ready(function () {
    $('div.product_categories_menu_wrapper > .product_categories_menu > li > a').addClass('menu_active');
    $('div.product_categories_menu_wrapper > .product_categories_menu > li > a').addClass('current-menu-ancestor');
    });
    </script>
<?php
if(in_array('ovens',$product_category_slugs)){
$productData = builder_get_product_components_json($post->ID);
$ralData = file_get_contents(get_template_directory() . '/woocommerce-assets/ral.json');
$sku_list = json_encode(get_option('wc_theme_sap_quantities')?:[]);
?>
<script type='text/javascript'>
    var productData = <?= $productData ?>;
    var RALdata = <?= $ralData ?>;
    var SKUList = <?=$sku_list?>;
</script>

<?php while ( have_posts() ) : ?>
	<?php the_post(); ?>
    <?php wc_get_template('builder/sub_header.php', $args); ?>
    <section class="section section--builder builder-section">
		<?php
		wc_get_template( 'builder/sidebar.php');
		wc_get_template( 'builder/constructor.php',$args);
		?>
    </section>
   <?php
    wc_get_template('builder/builder_steps/select_retailer.php', $args);
    wc_get_template('builder/modal_build.php');
    wc_get_template('builder/modal_confirmation.php');
    wc_get_template('builder/modal_confirmation_build.php');
    wc_get_template('builder/modal_order_details.php');
    wc_get_template('builder/modal_processing.php');

    endwhile; // end of the loop.
    get_footer('woocommerce');
    }else {

    /**
     * woocommerce_before_main_content hook.
     *
     * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
     * @hooked woocommerce_breadcrumb - 20
     */
    do_action('woocommerce_before_main_content');

    while (have_posts()) :
        the_post();
        wc_get_template('products/product.php', $args);

    endwhile; // end of the loop.

    /**
     * woocommerce_after_main_content hook.
     *
     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
     */
    do_action('woocommerce_after_main_content');

    /**
     * woocommerce_sidebar hook.
     *
     * @hooked woocommerce_get_sidebar - 10
     */
    do_action('woocommerce_sidebar');

    get_footer('agency');
}

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
