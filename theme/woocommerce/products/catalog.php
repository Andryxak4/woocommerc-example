<main class="catalog">
    <div class="catalog__cover" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/woocommerce-assets/images/shop-cover.png')">
        <h1 class="title"><?php woocommerce_page_title(); ?></h1>
    </div>
    <div class="catalog__top-block">
        <div class="container">
            <div class="catalog__top-block-row">
                <div class="catalog__top-block-item">
                    <h2>Title</h2>
                    <h3>Subtitle</h3>
                    <p>Text</p>
                </div>
                <div class="catalog__top-block-item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/woocommerce-assets/images/doors.svg" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="section section--catalog">
        <div class="container">

            <!-- filter panel start -->
            <div class="filter-panel-main">
                <div class="refine">
                    <a href="javascript:;"> <i class="fa fa-bars"></i> <span class="refine-text">refine</span></a>
                </div>
                <div class="filter-panel-aside__title">refine</div>
                <div class="per-page-block">
                    <?php products_per_page_filter_html();?>
                    <div class="results">
                        <?php woocommerce_result_count();?>
                    </div>
                </div>
               <?php woocommerce_pagination();?>
            </div>
            <!-- End filter panel start -->

            <div class="catalog__row">
                <div class="filter-panel-aside refine-left-menu open">
                   <?php
                   if (    wc_theme_is_user_have_capability('sales_consultant') ||
                           wc_theme_is_user_have_capability('showroom_manager') ||
                           wc_theme_is_user_have_capability('business_admin')) {
                       agency_products_filter_html();
                   }
                   ?>
                    <?php if (function_exists('dynamic_sidebar')) {
                        dynamic_sidebar('catalog-sidebar');
                    } ?>
                </div>
                
               
               
         
                <div class="catalog__main-content">
             
                    <!-- End filter panel start -->
                    <div class="cards">
                        <?php
                        while (have_posts()){
                            the_post();
	                        do_action( 'woocommerce_shop_loop' );
                            $product_obj = wc_get_product(get_the_ID());
	                        $product_fields = get_fields(get_the_ID());
                            if(wc_products_array_filter_visible($product_obj)){
                                ?>
                                <div class="cards__item">
                                    <div class="card oven-card">
                                        <?=wc_theme_up_to_sale_banner($product_obj); ?>
                                        <div class="card__picture">
                                  <a href="<?php echo $product_obj->get_permalink();?>">
                                      <img src="<?php echo get_the_post_thumbnail_url();?>" alt="">
                                  </a>
                                        </div>

                                        <span class="title">
                                            <?php
                                            $sku = $product_obj->get_sku() ?: $product_fields['product_sku'];
                                            if(!empty($sku)){
	                                            echo $sku;
                                            }
                                            ?>
                                        </span>
                                        <a href="<?php echo $product_obj->get_permalink();?>">
                                            <span class="subtitle"><?php echo $product_obj->get_name();?></span>
                                        </a>
                                      
                                    </div>
                                    <a href="<?php echo $product_obj->get_permalink();?>"><button class="button button--light button_permalink" value="<?php echo $product_obj->get_permalink();?>">More Information</button></a>
                                </div>
                        <?php
                            }
                        }
                        ?>
                    </div>
                    <div class="filter-panel-main filter-panel-main--footer">
                        
                        <?php products_per_page_filter_html();?>
                        <?php woocommerce_pagination();?>
                
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script type="text/javascript">
    $('.button_permalink').on('click', function () {
       location.assign($(this).val());
    });
    $(document).ready(function () {
        $('div.product_categories_menu_wrapper > .product_categories_menu > li > a').addClass('menu_active');
    });
</script>