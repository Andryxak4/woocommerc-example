<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined( 'ABSPATH' ) || exit;
?>

	<?php if ( true === WC()->cart->needs_shipping_address() ) : ?>


		 <div class="form__group">

			<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>


                <div class="form__row">
					<?php woocommerce_form_field('shipping_first_name', [
						'class' => ['form__field'],
						'label' => 'First Name',
						'label_class' => 'form__label',
						'required' => true,
						'custom_attributes' => ['required' => 'required', 'readonly' => 'readonly', 'onfocus' => "this.removeAttribute('readonly');"],
					]); ?>
					<?php woocommerce_form_field('shipping_last_name', [
						'class' => ['form__field'],
						'label' => 'Last Name',
						'label_class' => 'form__label',
						'required' => true,
						'custom_attributes' => ['required' => 'required', 'readonly' => 'readonly', 'onfocus' => "this.removeAttribute('readonly');"],
					]); ?>
                </div>
             <div class="form__row">
                 <?php woocommerce_form_field('shipping_company', [
                     'class' => ['form__field'],
                     'label' => 'Company Name',
                     'label_class' => 'form__label',
                     'custom_attributes' => ['readonly' => 'readonly', 'onfocus' => "this.removeAttribute('readonly');"],
                 ]); ?>
             </div>
                <div class="form__row">
					<?php woocommerce_form_field('shipping_phone', [
						'type' => 'tel',
						'class' => ['form__field'],
						'label' => 'Mobile Number',
						'label_class' => 'form__label',
						'required' => true,
						'custom_attributes' => ['required' => 'required', 'maxlength' => '10','readonly' => 'readonly', 'onfocus' => "this.removeAttribute('readonly');"],
					]); ?>
					<?php woocommerce_form_field('shipping_email', [
						'type' => 'email',
						'class' => ['form__field'],
						'label' => 'Email Address',
						'label_class' => 'form__label',
						'required' => true,
						'custom_attributes' => ['required' => 'required','readonly' => 'readonly', 'onfocus' => "this.removeAttribute('readonly');"],
					]); ?>
                </div>
                <div class="form__row">
					<?php woocommerce_form_field('shipping_address_1', [
						'class' => ['form__field'],
						'label' => 'Street address 1',
						'label_class' => 'form__label',
						'placeholder' => 'House number and street',
						'required' => true,
						'custom_attributes' => ['required' => 'required', 'readonly' => 'readonly', 'onfocus' => "this.removeAttribute('readonly');"],
					]); ?>
                </div>
                <div class="form__row">
					<?php woocommerce_form_field('shipping_address_2', [
						'class' => ['form__field'],
						'label' => 'Street address 2',
						'label_class' => 'form__label',
						'placeholder' => 'Apartment, suite, unit, building, floor, etc.',
                        'custom_attributes' => ['readonly' => 'readonly', 'onfocus' => "this.removeAttribute('readonly');"]
					]); ?>
                </div>
                <div class="form__row">
                    <?php wc_theme_woocommerce_form_field('shipping_country', [
                        'type' => 'country',
                        'label' => 'Country',
                        'label_class' => 'form__label',
                        'required' => true,
                        'class' => ['form__field', 'dark'],
                        'default' => "AU",
                        'custom_attributes' => ['required' => 'required', 'data-minimum-results-for-search' => "Infinity", 'readonly' => 'readonly', 'onfocus' => "this.removeAttribute('readonly');"],
                        'input_class' => ['custom-select']
                    ]); ?>
                    <?php woocommerce_form_field('shipping_state', [
                        'type' => 'state',
                        'class' => ['form__field', 'dark'],
                        'label' => 'State',
                        'label_class' => 'form__label',
                        'required' => true,
                        'custom_attributes' => ['required' => 'required', 'data-minimum-results-for-search' => "Infinity", 'readonly' => 'readonly', 'onfocus' => "this.removeAttribute('readonly');"],
                        'input_class' => ['custom-select']
                    ]); ?>
                </div>
                <div class="form__row">
                    <?php woocommerce_form_field('shipping_city', [
                        'class' => ['form__field'],
                        'label' => 'Suburb',
                        'label_class' => 'form__label',
                        'required' => true,
                        'custom_attributes' => ['required' => 'required'],
                    ]); ?>
					<?php woocommerce_form_field('shipping_postcode', [
						'class' => ['form__field'],
						'label' => 'Postcode',
						'label_class' => 'form__label',
						'required' => true,
						'custom_attributes' => ['required' => 'required', 'readonly' => 'readonly', 'onfocus' => "this.removeAttribute('readonly');"],
					]); ?>
			</div>
             <span>*This field are mandatory</span>
			<?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

		</div>
	<?php endif; ?>

