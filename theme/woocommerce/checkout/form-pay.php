<?php
/**
 * Pay for order form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-pay.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined('ABSPATH') || exit;

$parent = wc_get_order($order->get_parent_id());
$totals = $parent->get_order_item_totals(); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
?>
<script>
    var order_id = <?php echo $parent->get_id(); ?>;
    $('.page__title').text('Payment');
    $('title').text('Payment');
</script>
<form id="shipping_address_form" method="post">
    <div class="checkout__item payment-form">
        <div class="payment__item">
        <div class="form__group">
                        <span class="form__title form__title--contact">
                            Contact
                            <button class="flat-square-btn green " type="button" data-modal="edit-contact-modal">
                                <i class="far fa-pen"></i>
                                <span>Edit</span>
                            </button>
                        </span>
                        <div class="payment__contact">
                            <div class="payment__contact-row">
                                <span class="payment__contact-title">Email</span>
                                <span class="payment__contact-value shipping_email"><?php echo get_post_meta($parent->get_id(), '_shipping_email', true); ?></span>
                            </div>
                            <div class="payment__contact-row">
                                <span class="payment__contact-title">Customers Address</span>
                                <span class="payment__contact-value shipping_address"><?php
                                    $shipping_fields = $parent->data['shipping'];
                                    if ($shipping_fields['address_2'] !== "") {
                                        $shipping_address = $shipping_fields['address_1']. ", " .
                                            $shipping_fields['address_2'] . ", " .
                                            $shipping_fields['city'] . ", " .
                                            $shipping_fields['state'] . " " .
                                            $shipping_fields['postcode'] . " " . "Australia";
                                    } else {
                                        $shipping_address = $shipping_fields['address_1']. ", " .
                                            $shipping_fields['city'] . ", " .
                                            $shipping_fields['state'] . " " .
                                            $shipping_fields['postcode'] . " " . "Australia";
                                    }
                                    echo $shipping_address;
                                    ?></span>
                            </div>
                            <div class="payment__contact-row">
                                <span class="payment__contact-title">Delivery Method</span>
                                <span class="payment__contact-value shipping_method"><?php echo $parent->get_shipping_method(); ?></span>
                                <span class="payment__contact-price shipping_price"><?php echo wc_theme_wc_price($parent->get_shipping_tax()); ?></span>
                            </div>
                        </div>
                    </div>
    <div class="form__group form__group--payment" id="payment">
    <span class="form__title">
                            Payment
                        </span>
        <span class="grey-subtitle">All transactions are secure and encrypted.</span>
        <?php if ($parent->needs_payment()) : ?>
            <div class="payment__card">
                <ul class="wc_payment_methods payment_methods methods">
                    <?php
                    if (!empty($available_gateways)) {
                        foreach ($available_gateways as $gateway) {
                            wc_get_template('checkout/payment-method.php', array('gateway' => $gateway));
                        }
                    } else {
                        echo '<li class="woocommerce-notice woocommerce-notice--info woocommerce-info">' . apply_filters('woocommerce_no_available_payment_methods_message', WC()->customer->get_billing_country() ? esc_html__('Sorry, it seems that there are no available payment methods for your state. Please contact us if you require assistance or wish to make alternate arrangements.', 'woocommerce') : esc_html__('Please fill in your details above to see available payment methods.', 'woocommerce')) . '</li>'; // @codingStandardsIgnoreLine
                    }
                    ?>
                </ul>
            </div>
        <?php endif; ?>
        <input type="hidden" name="woocommerce_pay" value="1" />
        <?php wc_get_template('checkout/terms.php'); ?>

        <?php do_action( 'woocommerce_pay_order_before_submit' ); ?>

        <?php echo apply_filters( 'woocommerce_pay_order_button_html', '<button type="submit" class="button button--dark" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine ?>
        <?php do_action( 'woocommerce_pay_order_after_submit' ); ?>

        <?php wp_nonce_field( 'woocommerce-pay', 'woocommerce-pay-nonce' ); ?>
    </div>
        </div>
    </div>
    <div class="checkout__item sticky-cards">
        <div class="checkout__card your-order-card">
            <div class="your-order-card__title"><?php esc_html_e('Your order', 'woocommerce'); ?>
            <span class="your-order-card__date"><?php
                $schedule_date =  $order->get_meta('_wc_deposits_partial_payment_date');
                echo date('F d, Y',$schedule_date);

                ?></span>
        </div>

            <?php if (count($parent->get_items()) > 0) : ?>
                <?php
                $composite_container = null;
                $composite_container_id = null;
                $total = 0;
                foreach ($parent->get_items() as $item_id => $item) :
                    $product_id = $item->get_product_id();
                    $_product = $item->get_product();

                    $is_oven = has_term('ovens', 'product_cat', $_product->get_id());
                    if($is_oven){
                        if(wc_cp_is_composite_container_order_item($item)) {

                            foreach (wc_cp_get_composited_order_items($item) as $composited_item){
                                if(has_term('oven-power-type', 'product_cat',$composited_item->get_product_id())){
                                    $power_type = wc_get_product($composited_item->get_product_id());
                                    $power_type_price = $power_type->get_price();
                                }
                                if(has_term('decorative-styling', 'product_cat', $composited_item->get_product_id())){
                                    if($composited_item->get_variation_id()){
                                        $decorative_styling = wc_get_product($composited_item->get_variation_id());
                                    }else{
                                        $decorative_styling = wc_get_product($composited_item->get_product_id());
                                    }
                                    $decorative_styling_price = $decorative_styling->get_price();
                                }
                            }
                            $composite_container = $item;
                            $composite_container_id = $item_id;
                        }
                    }

                    $is_cooktop = has_term('cooktop', 'product_cat', $product_id);
                    $product_categories = get_the_terms($product_id, 'product_cat'); //Array of Object WP_TERM{}
                    $product_first_category = $product_categories[0]->slug;
                    ?>
                    <?php
                    if (!apply_filters('woocommerce_order_item_visible', true, $item)) {
                        continue;
                    }
                    if ($_product->get_price() != '0') {
                        //exclude terms
                        $is_power_type = has_term('oven-power-type', 'product_cat', $_product->get_id());
                        $is_gas_type = has_term('gas-type', 'product_cat', $_product->get_id());
                        $is_range_hood_series = has_term('range-hood-series', 'product_cat', $_product->get_id());
                        $is_base_options = has_term('base-options', 'product_cat', $_product->get_id());
                        $is_decorative_styling = has_term('decorative-styling', 'product_cat', $_product->get_id()) ||
                            has_term('decorative-styling', 'product_cat', $_product->get_parent_id());
                        $is_cooktop = has_term('cooktop', 'product_cat', $_product->get_id());
                        if ($is_base_options || $is_gas_type || $is_oven || $is_power_type || $is_range_hood_series || $is_decorative_styling) {
                            continue;
                        } else {
                            if($is_cooktop) {
                                $args['composite_container'] = $composite_container;
                                $args['composite_container_id'] = $composite_container_id;
                                if(isset($power_type_price)) {
                                    $args['power_type_price'] = $power_type_price;
                                }
                                if(isset($decorative_styling_price)) {
                                    $args['decorative_styling_price'] = $decorative_styling_price;
                                }
                            }
                          ?>

                            <a href="#" class="your-order-card__product">
                                <img class="your-order-card__product-img"
                                     src="<?=wp_get_attachment_image_url($_product->image_id);?>" class=""
                                     alt="">
                                <div class="your-order-card__product-name">
                                    <?php
                                    echo apply_filters('woocommerce_order_item_name', esc_html($item->get_name()), $item, false); // @codingStandardsIgnoreLine

                                    do_action('woocommerce_order_item_meta_start', $item_id, $item, $parent, false);

                                    wc_display_item_meta($item);

                                    do_action('woocommerce_order_item_meta_end', $item_id, $item, $parent, false);
                                    ?>
                                </div>
                                    <div class="your-order-card__product-price">
                                    <?php
                                    if($is_cooktop && $composite_container && $composite_container_id){
                                        $price = $parent->get_line_total($item);
                                        $complex_price = $price + $power_type_price + $decorative_styling_price;
                                        ?>
                                        <span class="current"><?php echo wc_theme_wc_price($complex_price);; ?></span>
                                        <?php
                                    }else{
                                        ?>
                                        <span class="current"><?php echo $parent->get_formatted_line_subtotal($item); ?></span>
                                    <?php } ?>
                                </div>
                            </a>

                            <?php
                            $composite_container = null;
                            $composite_id = null;
                            $composite_container_cart_item = null;
                            $composite_container_cart_item_key = null;
                        }
                    }
                 endforeach; ?>
            <?php endif; ?>

            <?php if ($totals) : ?>
                <?php foreach ($totals as $total) : ?>
                    <?php if($total['label'] == "Total:" || $total['label'] == "To Pay"){ ?>
                        <div class="your-order-card__price-row">
                            <div class="total-title"><?php echo $total['label']; ?></div>
                            <div class="total-price"><?php echo $total['value']; ?></div>
                        </div>
                    <?php }else{ ?>
                        <div class="your-order-card__price-row">
                        <span class="title">
                            <?php echo $total['label']; ?>
                        </span>
                            <div class="price">
                                <?php echo $total['value']; ?>
                            </div>
                        </div>
                    <?php } ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
</form>
<div class="modal full" id="edit-contact-modal">
    <div class="modal__wrapper">

        <div class="modal__body">
            <div class="">
                <div class="edit-contact__content">
                    <div class="form__group">
                            <span class="form__title">
                                Contact
                            </span>

                        <div class="form__row">
                            <div class="form__field">
                                <?php woocommerce_form_field('shipping_phone', [
                                    'type' => 'tel',
                                    'class' => ['form__field'],
                                    'label' => 'Mobile Number',
                                    'label_class' => 'form__label',
                                    'required' => true,
                                    'default' => get_post_meta($parent->get_id(), '_shipping_phone', true),
                                    'custom_attributes' => ['required' => 'required', 'maxlength' => '10',],
                                ]); ?>
                                <span class="error-massage">Mobile number field is required</span>
                            </div>
                            <div class="form__field">
                                <?php woocommerce_form_field('shipping_email', [
                                    'type' => 'email',
                                    'class' => ['form__field'],
                                    'label' => 'Email Address',
                                    'label_class' => 'form__label',
                                    'required' => true,
                                    'default' => get_post_meta($parent->get_id(), '_shipping_email', true),
                                    'custom_attributes' => ['required' => 'required',],
                                ]);
                                ?>
                                <span class="error-massage">Email address field is required</span>
                            </div>
                        </div>

                        <div class="form__row">
                            <div class="form__field">
                                <?php woocommerce_form_field('shipping_address_1', [
                                    'class' => ['form__field'],
                                    'label' => 'Street address 1',
                                    'label_class' => 'form__label',
                                    'placeholder' => 'House number and street',
                                    'required' => true,
                                    'default' => $parent->get_shipping_address_1(),
                                    'custom_attributes' => ['required' => 'required',],
                                ]); ?>
                            </div>
                        </div>
                        <div class="form__row">
                            <div class="form__field">
                                <?php woocommerce_form_field('shipping_address_2', [
                                    'class' => ['form__field'],
                                    'label' => 'Street address 2',
                                    'label_class' => 'form__label',
                                    'default' => $parent->get_shipping_address_2(),
                                    'placeholder' => 'Apartment, suite, unit, building, floor, etc.',
                                ]); ?>
                            </div>
                        </div>
                        <div class="form__row">
                            <div class="form__field dark">
                                <?php wc_theme_woocommerce_form_field('shipping_country', [
                                    'type' => 'country',
                                    'label' => 'Country',
                                    'label_class' => 'form__label',
                                    'required' => true,
                                    'class' => ['form__field', 'dark'],
                                    'default' => $parent->get_shipping_country(),
                                    'custom_attributes' => ['required' => 'required', 'data-minimum-results-for-search' => "Infinity"],
                                    'input_class' => ['custom-select']
                                ]); ?>
                            </div>
                            <div class="form__field dark">
                                <?php woocommerce_form_field('shipping_state', [
                                    'type' => 'state',
                                    'class' => ['form__field', 'dark'],
                                    'label' => 'State',
                                    'label_class' => 'form__label',
                                    'required' => true,
                                    'default' => $parent->get_shipping_state(),
                                    'custom_attributes' => ['required' => 'required', 'data-minimum-results-for-search' => "Infinity"],
                                    'input_class' => ['custom-select']
                                ]); ?>
                            </div>

                        </div>
                        <div class="form__row">
                            <div class="form__field">
                                <?php woocommerce_form_field('shipping_city', [
                                    'class' => ['form__field'],
                                    'label' => 'Suburb',
                                    'label_class' => 'form__label',
                                    'required' => true,
                                    'default' => $parent->get_shipping_city(),
                                    'custom_attributes' => ['required' => 'required'],
                                    ''
                                ]); ?>
                                <span class="error-massage"></span>
                            </div>
                            <div class="form__field">
                                <?php woocommerce_form_field('shipping_postcode', [
                                    'class' => ['form__field'],
                                    'label' => 'Postcode',
                                    'label_class' => 'form__label',
                                    'required' => true,
                                    'default' => $parent->get_shipping_postcode(),
                                    'custom_attributes' => ['required' => 'required'],
                                ]); ?>
                                <span class="error-massage"></span>
                            </div>
                        </div>
                        <span>*This field are mandatory</span>
                    </div>
                    <button class="button button--dark second-payment-edit-contact-button" type="button">confirm changes</button>
                </div>
            </div>

        </div>
        <span class="modal__close"></span>
    </div>

</div>