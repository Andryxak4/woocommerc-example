<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.8.0
 */
defined('ABSPATH') || exit;

do_action('woocommerce_before_cart');
?>
<div class="cart__content ">

    <form class="cart__row" action="<?php echo esc_url(wc_get_cart_url()); ?>" method="post">
        <?php do_action('woocommerce_before_cart_table'); ?>
        <div class="cart__item">
            <table class="products-table cart-table" cellspacing="0">
                <thead>
                    <tr>
                        <th></th>
                        <th><?php esc_html_e('Product', 'woocommerce'); ?></th>
                        <th><?php esc_html_e('Price', 'woocommerce'); ?></th>
                        <th><?php esc_html_e('Quantity', 'woocommerce'); ?></th>
                        <th><?php esc_html_e('Subtotal', 'woocommerce'); ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php do_action('woocommerce_before_cart_contents'); ?>

                    <?php
                    $composite_container = null;
                    $composite_container_id = null;
                    $composite_container_cart_item = null;
                    $composite_container_cart_item_key = null;
                    foreach (WC()->cart->get_cart() as $cart_item_key => $cart_item) {
                        $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                        $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                        if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                            $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
                            $is_oven = has_term('ovens', 'product_cat', $_product->get_id());
                            if ($is_oven) {
                                if (wc_cp_is_composite_container_cart_item($cart_item)) {
                                    foreach ($cart_item['composite_data'] as $composite_data) {
                                        if (has_term('oven-power-type', 'product_cat', $composite_data['product_id'])) {
                                            $power_type = wc_get_product($composite_data['product_id']);
                                            $power_type_price = $power_type->get_price();
                                        }
                                        if (has_term('decorative-styling', 'product_cat', $composite_data['product_id'])) {
                                            $decorative_styling = wc_get_product($composite_data['product_id']);
                                            $decorative_styling_price = $decorative_styling->get_price();
                                        }
                                    }
                                    $composite_container = $_product;
                                    $composite_container_id = $product_id;
                                    $composite_container_cart_item = $cart_item;
                                    $composite_container_cart_item_key = $cart_item_key;
                                }
                            }
                            if ($_product->get_price() != '0') {
                                //exclude terms
                                $is_power_type = has_term('oven-power-type', 'product_cat', $_product->get_id());
                                $is_gas_type = has_term('gas-type', 'product_cat', $_product->get_id());
                                $is_range_hood_series = has_term('range-hood-series', 'product_cat', $_product->get_id());
                                $is_base_options = has_term('base-options', 'product_cat', $_product->get_id());
                                $is_decorative_styling = has_term('decorative-styling', 'product_cat', $_product->get_id()) ||
                                        has_term('decorative-styling', 'product_cat', $_product->get_parent_id());
                                $is_cooktop = has_term('cooktop', 'product_cat', $_product->get_id());
                                $args = [
                                    'cart_item' => $cart_item,
                                    'cart_item_key' => $cart_item_key,
                                    'product_id' => $product_id,
                                    '_product' => $_product,
                                    'product_permalink' => $product_permalink,
                                ];
                                if ($is_base_options || $is_gas_type || $is_oven || $is_power_type || $is_range_hood_series || $is_decorative_styling) {
                                    continue;
                                } else {
                                    if ($is_cooktop) {
                                        $args['composite_container'] = $composite_container;
                                        $args['composite_container_id'] = $composite_container_id;
                                        $args['composite_container_cart_item'] = $composite_container_cart_item;
                                        $args['composite_container_cart_item_key'] = $composite_container_cart_item_key;
                                        if (isset($power_type_price)) {
                                            $args['power_type_price'] = $power_type_price;
                                        }
                                        if (isset($decorative_styling_price)) {
                                            $args['decorative_styling_price'] = $decorative_styling_price;
                                        }
                                    }
                                    wc_get_template(
                                            'cart/cart-details-item.php', $args
                                    );
                                    $composite_container = null;
                                    $composite_id = null;
                                    $composite_container_cart_item = null;
                                    $composite_container_cart_item_key = null;
                                }
                            }
                        }
                    }
                    ?>
                </tbody>
            </table>
            <?php do_action('woocommerce_cart_contents'); ?>
            <?php if (wc_coupons_enabled()) { ?>
                <div class="cupon-box">
                    <div class="cupon-title">If you have a coupon code, please apply it below.</div>
                    <div class="cupon-row">
                        <input class="light-input" type="text" name="coupon_code" id=""
                               placeholder="<?php esc_attr_e('Coupon code', 'woocommerce'); ?>">
                        <button type="submit" class="button button--light apply_coupon" name="apply_coupon"
                                value="<?php esc_attr_e('Apply coupon', 'woocommerce'); ?>"><?php esc_attr_e('Apply coupon', 'woocommerce'); ?></button>
                                <?php do_action('woocommerce_cart_coupon'); ?>
                    </div>
                </div>
            <?php } ?>
        </div>

        <?php do_action('woocommerce_cart_actions'); ?>

        <?php wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce'); ?>


        <?php do_action('woocommerce_after_cart_contents'); ?>

        <?php do_action('woocommerce_after_cart_table'); ?>

        <div class="cart__item">


            <?php do_action('woocommerce_before_cart_collaterals'); ?>


            <div class="cart-summary">
                <?php
                /**
                 * Cart collaterals hook.
                 *
                 * @hooked woocommerce_cross_sell_display
                 * @hooked woocommerce_cart_totals - 10
                 */
                do_action('woocommerce_cart_collaterals');
                ?>
            </div>

            <?php
            wc_theme_cross_sell_display();
            ?>

            <a href="<?= get_site_url(); ?>" class="button button--light continue-shopping">Continue
                shopping <i class="far fa-arrow-right"></i></a>
        </div>
        <?php do_action('woocommerce_after_cart'); ?>
    </form>
</div>