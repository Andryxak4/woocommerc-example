<?php
$total = wc_price(WC()->cart->get_subtotal());
?>
<!-- Product page > Sidebar - Cart -->
<div class="sidebar-cart">
    <div class="sidebar-cart__header">
        <div class="sidebar-cart__title">Shopping cart</div>
        <button class="sidebar-cart__close js-toggle-cart"></button>
    </div>
    <?php if (empty($cart)) { ?>
        <!-- Empty state -->
        <div class="empty-state">
            <div class="empty-cart"></div>
            <span class="empty-text">Your cart is currently empty.</span>
        </div>
        <!-- End empty state -->
    <?php } else { ?>
        <!-- with products -->
        <div class="sidebar-cart__content">
            <ul class="sidebar-cart__list">
                <?php
                foreach ($cart as $cart_item_key => $cart_item) {
                    $_product = apply_filters('woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key);
                    $product_id = apply_filters('woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key);

                    if ($_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters('woocommerce_cart_item_visible', true, $cart_item, $cart_item_key)) {
                        $product_permalink = apply_filters('woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink($cart_item) : '', $cart_item, $cart_item_key);
                        $is_oven = has_term('ovens', 'product_cat', $_product->get_id());
                        if ($is_oven) {
                            if (wc_cp_is_composite_container_cart_item($cart_item)) {
                                foreach ($cart_item['composite_data'] as $composite_data) {
                                    if (has_term('oven-power-type', 'product_cat', $composite_data['product_id'])) {
                                        $power_type = wc_get_product($composite_data['product_id']);
                                        $power_type_price = $power_type->get_price();
                                    }
                                    if (has_term('decorative-styling', 'product_cat', $composite_data['product_id'])) {
                                        $decorative_styling = wc_get_product($composite_data['product_id']);
                                        $decorative_styling_price = $decorative_styling->get_price();
                                    }
                                }
                                $composite_container = $_product;
                                $composite_container_id = $product_id;
                                $composite_container_cart_item = $cart_item;
                                $composite_container_cart_item_key = $cart_item_key;
                            }
                        }
                        if ($_product->get_price() != '0') {
                            //exclude terms
                            $is_power_type = has_term('oven-power-type', 'product_cat', $_product->get_id());
                            $is_gas_type = has_term('gas-type', 'product_cat', $_product->get_id());
                            $is_range_hood_series = has_term('range-hood-series', 'product_cat', $_product->get_id());
                            $is_base_options = has_term('base-options', 'product_cat', $_product->get_id());
                            $is_decorative_styling = has_term('decorative-styling', 'product_cat', $_product->get_id()) ||
                                has_term('decorative-styling', 'product_cat', $_product->get_parent_id());
                            $is_cooktop = has_term('cooktop', 'product_cat', $_product->get_id());
                            $args = [
                                'cart_item' => $cart_item,
                                'cart_item_key' => $cart_item_key,
                                'product_id' => $product_id,
                                '_product' => $_product,
                                'product_permalink' => $product_permalink,
                            ];
                            if ($is_base_options || $is_gas_type || $is_oven || $is_power_type || $is_range_hood_series || $is_decorative_styling) {
                                continue;
                            } else {
                                if ($is_cooktop) {
                                    $args['composite_container'] = $composite_container;
                                    $args['composite_container_id'] = $composite_container_id;
                                    $args['composite_container_cart_item'] = $composite_container_cart_item;
                                    $args['composite_container_cart_item_key'] = $composite_container_cart_item_key;
                                    if (isset($power_type_price)) {
                                        $args['power_type_price'] = $power_type_price;
                                    }
                                    if (isset($decorative_styling_price)) {
                                        $args['decorative_styling_price'] = $decorative_styling_price;
                                    }
                                }
                                wc_get_template(
                                    'cart/sidebar-cart-details-item.php',
                                    $args
                                );
                                $composite_container = null;
                                $composite_id = null;
                                $composite_container_cart_item = null;
                                $composite_container_cart_item_key = null;
                            }
                        }
                    }
                } ?>
            </ul>
            <div class="sidebar-cart__footer">
                <div class="sidebar-cart__total-price">
                    <div class="subtitle">SUBTOTAL:</div>
                    <span class="price"><?= $total; ?></span>
                </div>
                <div class="sidebar-cart__buttons">
                    <a class="button button--dark" href="<?= wc_get_checkout_url(); ?>">checkout</a>
                    <a class="button button--light" href="<?= wc_get_cart_url(); ?>">View cart</a>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
<!-- END Product page > Sidebar - Cart -->