<?php

namespace FrontApi\Inc\Api;

use FrontApi\Inc\Api\Callbacks\AjaxCallbacks;

/**
 * Class Ajax
 *
 * @package FrontApi\Inc\Api
 */
class Ajax {

	/**
	 * Ajax constructor.
	 */
	public function __construct() {
		$this->registerAdminAjaxHooks();
		$this->registerFrontAjaxHooks();
	}

	/**
	 * Register ajax hooks for admin side.
	 */
	public function registerAdminAjaxHooks(): void {
		
	}

	/**
	 * Register ajax hooks for frontend side.
	 */
	public function registerFrontAjaxHooks(): void {

		// The action of receiving a subscription lying in the cart.
		add_action( 'wp_ajax_get_cart_subscription_plan', array( AjaxCallbacks::class, 'getCartSubscriptionPlan' ) );
		add_action( 'wp_ajax_nopriv_get_cart_subscription_plan', array( AjaxCallbacks::class, 'getCartSubscriptionPlan' ) );

		// Get list of available subscription.
		add_action( 'wp_ajax_get_subscriptions_list', array( AjaxCallbacks::class, 'getSubscriptionsList' ) );
		add_action( 'wp_ajax_nopriv_get_subscriptions_list', array( AjaxCallbacks::class, 'getSubscriptionsList' ) );

		// Add copy active subscription to cart.
		add_action( 'wp_ajax_copy_subscription_to_cart', array( AjaxCallbacks::class, 'copySubscriptionToCart' ) );

		// Add subscription to cart.
		add_action( 'wp_ajax_add_subscription_to_cart', array( AjaxCallbacks::class, 'addSubscriptionToCart' ) );
		add_action( 'wp_ajax_nopriv_add_subscription_to_cart', array( AjaxCallbacks::class, 'addSubscriptionToCart' ) );

		// Get composite product by product ID.
		add_action( 'wp_ajax_get_composite_product', array( AjaxCallbacks::class, 'getCompositeProduct' ) );
		add_action( 'wp_ajax_nopriv_get_composite_product', array( AjaxCallbacks::class, 'getCompositeProduct' ) );

		// Get list of composite products from cart.
		add_action( 'wp_ajax_get_cart_products', array( AjaxCallbacks::class, 'getCartProducts' ) );
		add_action( 'wp_ajax_nopriv_get_cart_products', array( AjaxCallbacks::class, 'getCartProducts' ) );

		// Edit cart product.
		add_action( 'wp_ajax_edit_cart_product', array( AjaxCallbacks::class, 'editCartProduct' ) );
		add_action( 'wp_ajax_nopriv_edit_cart_product', array( AjaxCallbacks::class, 'editCartProduct' ) );

		// Delete product from cart.
		add_action( 'wp_ajax_delete_cart_product', array( AjaxCallbacks::class, 'deleteCartProduct' ) );
		add_action( 'wp_ajax_nopriv_delete_cart_product', array( AjaxCallbacks::class, 'deleteCartProduct' ) );

		// Get cut off date of order changing.
		add_action( 'wp_ajax_get_cut_off_date', array( AjaxCallbacks::class, 'getCutOffDate' ) );

		// Get composite product list.
		add_action( 'wp_ajax_get_composite_products_list', array( AjaxCallbacks::class, 'getCompositeProductsList' ) );
		add_action( 'wp_ajax_nopriv_get_composite_products_list', array( AjaxCallbacks::class, 'getCompositeProductsList' ) );

		// ADD composite or simple product to cart.
		add_action( 'wp_ajax_add_product_to_cart', array( AjaxCallbacks::class, 'addProductToCart' ) );
		add_action( 'wp_ajax_nopriv_add_product_to_cart', array( AjaxCallbacks::class, 'addProductToCart' ) );

		// Apply coupon.
		add_action( 'wp_ajax_apply_coupon', array( AjaxCallbacks::class, 'applyCoupon' ) );
		add_action( 'wp_ajax_nopriv_apply_coupon', array( AjaxCallbacks::class, 'applyCoupon' ) );

		// Get delivery label.
		add_action( 'wp_ajax_get_delivery_label', array( AjaxCallbacks::class, 'ajaxGetDeliveryLabel' ) );
		add_action( 'wp_ajax_nopriv_get_delivery_label', array( AjaxCallbacks::class, 'ajaxGetDeliveryLabel' ) );

		// Get delivery address.
		add_action( 'wp_ajax_get_delivery_address', array( AjaxCallbacks::class, 'getDeliveryAddress' ) );
		add_action( 'wp_ajax_nopriv_get_delivery_address', array( AjaxCallbacks::class, 'getDeliveryAddress' ) );

		// Update delivery address.
		add_action( 'wp_ajax_update_delivery_address', array( AjaxCallbacks::class, 'updateDeliveryAddress' ) );
		add_action( 'wp_ajax_nopriv_update_delivery_address', array( AjaxCallbacks::class, 'updateDeliveryAddress' ) );

		// Get user's payment information.
		add_action( 'wp_ajax_get_payment_info', array( AjaxCallbacks::class, 'getPaymentInfo' ) );
		add_action( 'wp_ajax_nopriv_get_payment_info', array( AjaxCallbacks::class, 'getPaymentInfo' ) );

		// Creates new payment card by payment menthod nonce
		add_action( 'wp_ajax_add_credit_card', array( AjaxCallbacks::class, 'addCreditCard' ) );
		add_action( 'wp_ajax_nopriv_add_credit_card', array( AjaxCallbacks::class, 'addCreditCard' ) );

		//take Subject and Description fields and sends email to contact@youplate.it
		add_action( 'wp_ajax_need_help', array( AjaxCallbacks::class, 'needHelp' ) );
		add_action( 'wp_ajax_nopriv_need_help', array( AjaxCallbacks::class, 'needHelp' ) );
		//Get current user's order
		add_action( 'wp_ajax_get_order', array( AjaxCallbacks::class, 'getOrder' ) );
		add_action( 'wp_ajax_nopriv_get_order', array( AjaxCallbacks::class, 'getOrder' ) );

		//Get user's list of orders for Order History page
		add_action( 'wp_ajax_get_orders_list', array( AjaxCallbacks::class, 'getOrdersList' ) );
		add_action( 'wp_ajax_nopriv_get_orders_list', array( AjaxCallbacks::class, 'getOrdersList' ) );

		// Returns orders, containing  a list of recipes with ranking
		add_action( 'wp_ajax_get_order_list_recipes', array( AjaxCallbacks::class, 'getOrderListRecipes' ) );
		add_action( 'wp_ajax_nopriv_wp_ajax_get_order_list_recipes', array( AjaxCallbacks::class, 'getOrderListRecipes' ) );

		//Get details for Order Details page
		add_action( 'wp_ajax_get_order_details', array( AjaxCallbacks::class, 'getOrderDetails' ) );
		add_action( 'wp_ajax_nopriv_get_order_details', array( AjaxCallbacks::class, 'getOrderDetails' ) );

		//Update account information
		add_action( 'wp_ajax_update_account_information', array( AjaxCallbacks::class, 'updateAccountInformation' ) );
		add_action( 'wp_ajax_nopriv_update_account_information', array( AjaxCallbacks::class, 'updateAccountInformation' ) );

		//Get user's list of taste preference tags
		add_action( 'wp_ajax_get_dietary_preferences', array( AjaxCallbacks::class, 'getDietaryPreferences' ) );
		add_action( 'wp_ajax_nopriv_get_dietary_preferences', array( AjaxCallbacks::class, 'getDietaryPreferences' ) );

		//Update user's list of taste preference tags
		add_action( 'wp_ajax_update_dietary_preferences', array( AjaxCallbacks::class, 'updateDietaryPreferences' ) );
		add_action( 'wp_ajax_nopriv_update_dietary_preferences', array( AjaxCallbacks::class, 'updateDietaryPreferences' ) );

		add_action( 'wp_ajax_get_if_user_placed_order', array( AjaxCallbacks::class, 'getIfUserPlacedOrder' ) );

		// Returns text notification reminders and ondemand ordrer notifications
		add_action( 'wp_ajax_get_notification_status', array( AjaxCallbacks::class, 'getNotificationStatus' ) );
		add_action( 'wp_ajax_nopriv_wp_ajax_get_notification_status', array( AjaxCallbacks::class, 'getNotificationStatus' ) );

		// Get user ratings for the specify products in order
		add_action( 'wp_ajax_get_ratings', array( AjaxCallbacks::class, 'getRatings' ) );
		add_action( 'wp_ajax_nopriv_wp_ajax_get_ratings', array( AjaxCallbacks::class, 'getRatings' ) );

		// Update user ratings for the specify products in order
		add_action( 'wp_ajax_update_ratings', array( AjaxCallbacks::class, 'updateRatings' ) );
		add_action( 'wp_ajax_nopriv_wp_ajax_update_ratings', array( AjaxCallbacks::class, 'updateRatings' ) );


		
}