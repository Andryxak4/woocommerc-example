<?php

namespace FrontApi\Inc\Api\Callbacks;

if (!class_exists('\Braintree\Exception')) {
	echo 'Please activate the Braintree gateway plugin.';
	die();
}
use \Braintree\Exception;
use FrontApi\Inc\Controllers\AjaxAdminController;
use FrontApi\Inc\Exceptions\FrontApiException;
use FrontApi\Inc\Helpers\ErrorStore;
use FrontApi\Inc\Helpers\Response;

/**
 * Class AjaxCallbacks
 *
 * @package FrontApi\Inc\Api\Callbacks
 */
class AjaxCallbacks {

	/**
	 * The function get the subscription in the cart end echo it.
	 */
	public static function getCartSubscriptionPlan(): void {
		static::checkNonce();
		static::checkWooCommerceExist();

		try {
			$controller = new AjaxAdminController();
			Response::sendData( $controller->getCartSubscriptionPlan() );
		} catch ( FrontApiException $e ) {
			Response::sendError( $e->getMessage(), $e->getCode() );
		}
	}

	/**
	 * Get list of subscriptions data.
	 *
	 * @apiParam int $nights_per_week     Nights quantity per week.
	 * @apiParam int $serve_box           Serve box quantity.
	 */
	public static function getSubscriptionsList(): void {
		static::checkNonce();
		static::checkWooCommerceExist();

		$nights_per_week = (int) ($_POST['nights_per_week'] ?? false);
		$serve_box       = (int) ($_POST['serve_box'] ?? false);
		$errors_store    = new ErrorStore();

		if ( $serve_box && ( ! is_numeric( $serve_box ) || 0 > $serve_box ) ) {
			// Bad request. Incorrect quantity of nights per week.
			Response::sendError( $errors_store->getErrorMessage( 141 ), 141 );
		}

		if ( $nights_per_week && ( ! is_numeric( $nights_per_week ) || 0 > $nights_per_week ) ) {
			// Bad request. Incorrect quantity of serve boxes.
			Response::sendError( $errors_store->getErrorMessage( 142 ), 142 );
		}

		$controller    = new AjaxAdminController();
		$subsctiptions = $controller->getSubscriptionsList( $nights_per_week, $serve_box );
		Response::sendData( $subsctiptions );
	}

	/**
	 * If user logged in and has a  active subscription, copy subscription to cart.
	 */
	public static function copySubscriptionToCart(): void {
		static::checkNonce();
		static::checkWooCommerceExist();
		static::checkWCSubscriptionExist();

		try {
			$controller = new AjaxAdminController();
			Response::sendData( $controller->copySubscriptionToCart() );
		} catch ( FrontApiException $ex ) {
			Response::sendError( $ex->getMessage(), $ex->getCode() );
		}
	}

	/**
	 * Add subscription product to cart by ID.
	 *
	 * The method remove all products in cart before add
	 * subscription product to cart.
	 *
	 * @apiParam int $subscription_id         Subscription product ID.
	 */
	public static function addSubscriptionToCart(): void {
		static::checkNonce();
		static::checkWooCommerceExist();
		$errors = new ErrorStore();

		if ( empty( $_POST['subscription_id'] ) || ! is_numeric( $_POST['subscription_id'] ) ) {
			// The request has incorrect product ID.
			Response::sendError( $errors->getErrorMessage( 110 ), 110 );
		}

		$subscription_id = (int) $_POST['subscription_id'];
		$controller      = new AjaxAdminController();

		try {
			$cart_key = $controller->addSubscriptionToCart( $subscription_id );
			Response::sendData( array( 'cart_key' => $cart_key ) );
		} catch ( FrontApiException $ex ) {
			Response::sendError( $ex->getMessage(), $ex->getCode() );
		}
	}

	/**
	 * Get composite product by product ID or slug.
	 *
	 * @apiParam int $product_id            Composite product ID.
	 * @apiParam int $product_slug          Composite product slug.
	 */
	public static function getCompositeProduct(): void {
		static::checkNonce();
		static::checkWooCommerceExist();
		static::checkWCCompositeProducts();
		$errors = new ErrorStore();

		try {
			if ( ! empty( $_POST['product_slug'] ) ) {
				$controller = new AjaxAdminController();

				Response::sendData( $controller->getCompositeProductBySlug( $_POST['product_slug'] ) );
			} elseif ( ! empty( $_POST['product_id'] ) || is_numeric( $_POST['product_id'] ) ) {
				$controller = new AjaxAdminController();

				Response::sendData( $controller->getCompositeProductByID( (int) $_POST['product_id'] ) );
			} else {
				// The request has incorrect product ID or slug.
				Response::sendError( $errors->getErrorMessage( 107 ), 107 );
			}
		} catch ( FrontApiException $e ) {
			Response::sendError( $e->getMessage(), $e->getCode() );
		}
	}

	/**
	 * Get list of products from cart.
	 */
	public static function getCartProducts(): void {
		static::checkNonce();
		static::checkWooCommerceExist();
		static::checkWCCompositeProducts();

		try {
			$controller = new AjaxAdminController();
			$data = $controller->getCartProducts();

			Response::sendData( $data );
		} catch ( FrontApiException $ex ) {
			Response::sendError( $ex->getMessage(), $ex->getCode() );
		}
	}

	/**
	 * Change composite or simple product in cart.
	 *
	 * @apiParam string $cart_key                     Cart key of composite product that should be deleted.
	 * @apiParam int    $quantity                     Meals quantity.
	 * @apiParam int    $protein_id                   ID of protein component product.
	 * @apiParam int    $servings_id                  ID of servings component product.
	 * @apiParam int    $extra                        Store of extra components.
	 * @apiParam int    $extra[0]                     A data of the extra component of the composite product. It's
	 *                                                repeatable parameter.
	 * @apiParam int    $extra[0]['id']       The ID of a chose extra product.
	 * @apiParam int    $extra[0]['quantity']         Quantity of a chose extra product.
	 */
	public static function editCartProduct(): void {
		self::checkNonce();
		self::checkWooCommerceExist();
		self::checkWCCompositeProducts();

		$error = new ErrorStore();

		if ( ! isset( $_POST['cart_key'] ) || ! is_string( $_POST['cart_key'] ) ) {
			// The request has incorrect product ID or slug.
			Response::sendError( $error->getErrorMessage( 107 ), 107 );
		}

		$cart_key    = (string) $_POST['cart_key'];
		$product_id  = WC()->cart->get_cart_item( $cart_key )['product_id'];
		$quantity    = (int) ($_POST['quantity'] ?? 1);
		$protein_id  = (int) ($_POST['protein_id'] ?? null);
		$servings_id = (int) ($_POST['servings_id'] ?? null);
		$extra       = (array) $_POST['extra'];
		$controller = new AjaxAdminController();

		try {

			$product = wc_get_product( $product_id );
			if (!$product) {
				Response::sendError( 'Product not found', 404 );
			}
			$result = '';
			if ( $product->is_type( 'simple' ) ) {
				$result = $controller->editSimpleCartProduct( $cart_key, $quantity );
			} elseif ( $product->is_type( 'composite' ) ) {
				$result = $controller->editCompositeCartProduct( $cart_key, $quantity, $protein_id, $servings_id, $extra );
			}

			Response::sendData([
				'cart_key' => $result,
				'cart_recurring_subtotal' => \Ypi_Order::get_recurring_subtotal(),
				'cart_subtotal' => WC()->cart->get_subtotal(),
			]);
		} catch ( FrontApiException $e ) {
			Response::sendError( $e->getMessage(), $e->getCode() );
		}
	}

	/**
	 * Delete composite or simple product from cart.
	 *
	 * @apiParam string $cart_key           Cart key of composite product that should be deleted.
	 */
	public static function deleteCartProduct(): void {
		self::checkNonce();
		self::checkWooCommerceExist();

		$error = new ErrorStore();

		if ( ! isset( $_POST['cart_key'] ) || ! is_string( $_POST['cart_key'] ) ) {
			// The request has incorrect product ID or slug.
			Response::sendError( $error->getErrorMessage( 107 ), 107 );
		}

		$cart_key   = (string) $_POST['cart_key'];
		$controller = new AjaxAdminController();

		try {
			$result = $controller->deleteCartProduct( $cart_key );

			Response::sendData( [
				'result' => $result,
				'cart_recurring_subtotal' => \Ypi_Order::get_recurring_subtotal(),
				'cart_subtotal' => WC()->cart->get_subtotal(),
			] );
		} catch ( FrontApiException $e ) {
			Response::sendError( $e->getMessage(), $e->getCode() );
		}
	}

	/**
	 * Get list of composite product data.
	 *
	 * @throws FrontApiException
	 */
	public static function getCompositeProductsList(): void {
		static::checkWooCommerceExist();
		static::checkWCCompositeProducts();

		$controller = new AjaxAdminController();

		try {
			Response::sendData( $controller->getCompositeProductsList() );
		} catch ( FrontApiException $e ) {
			Response::sendError( $e->getMessage(), $e->getCode() );
		}
	}

	public static function getCutOffDate(): void {
		static::checkNonce();

		$controller = new AjaxAdminController();
		Response::sendData( $controller->getCutOffDate() );
	}

	/**
	 * Add composite or simple product to cart.
	 *
	 * @apiParam int   product_id                   The composite product ID.
	 * @apiParam int   quantity                     Meals quantity.
	 * @apiParam int   protein_id                   ID of protein component product.
	 * @apiParam int   servings_id                  ID of servings component product.
	 * @apiParam int   extra                        Store of extra components.
	 * @apiParam int   extra[0]                     A data of the extra component of the composite product. It's
	 *                                              repeatable parameter.
	 * @apiParam int   extra[0]['id']       The ID of a chose extra product.
	 * @apiParam int   extra[0]['quantity']         Quantity of a chose extra product.
	 */
	public static function addProductToCart(): void {
		self::checkNonce();
		self::checkWooCommerceExist();
		self::checkWCCompositeProducts();

		$error = new ErrorStore();

		if ( ! isset( $_POST['product_id'] ) || ! is_numeric( $_POST['product_id'] ) ) {
			// The request has incorrect product ID or slug.
			Response::sendError( $error->getErrorMessage( 107 ), 107 );
		}

		$id = (int) $_POST['product_id'];
		$product     = wc_get_product( $id );
		if (!$product) {
			Response::sendError( 'Product not found', 404 );
		}
		$quantity    = (int) ($_POST['quantity'] ?? 1);
		$protein_id  = (int) ($_POST['protein_id'] ?? 0);
		$servings_id = (int) ($_POST['servings_id'] ?? 0);
		$extra       = (array) $_POST['extra'] ?? [];
		$controller  = new AjaxAdminController();

		try {
			$result = $controller->addProductToCart( $product, $quantity, $protein_id, $servings_id, $extra );
			Response::sendData( array(
				'cart_key' => $result,
				'cart_recurring_subtotal' => \Ypi_Order::get_recurring_subtotal(),
				'cart_subtotal' => WC()->cart->get_subtotal(),
			) );
		} catch ( FrontApiException $e ) {
			Response::sendError( $e->getMessage(), $e->getCode() );
		}
	}

	/**
	 * Apply WooCommerce coupon.
	 *
	 * @apiParam string $coupon        WooCommerce coupon code.
	 */
	public static function applyCoupon(): void {
		self::checkNonce();
		self::checkWooCommerceExist();
		$error = new ErrorStore();

		if ( empty( $_POST['coupon'] ) ) {
			// The request has no coupon code.
			Response::sendError( $error->getErrorMessage( 109 ), 109 );
		}

		global $woocommerce;
		$coupon = sanitize_text_field( $_POST['coupon'] );
		$result = $woocommerce->cart->add_discount( $coupon );

		if ( $result ) {
			Response::sendData( array( 'Coupon applied' ) );
		} else {
			// The coupon is wrong or out of date.
			Response::sendError( $error->getErrorMessage( 115 ), 115 );
		}
	}

	/**
	 *  Get delivery label.
	 */
	public static function ajaxGetDeliveryLabel(): void {
		self::checkNonce();

		$delivery_label = \Ypi_Order::get_current_week_delivery_date();

		Response::sendData( array( 'label' => $delivery_label ) );
	}

	/**
	 *  Get delivery address.
	 */
	public static function getDeliveryAddress(): void {
		static::checkNonce();
		static::checkWooCommerceExist();

		$controller = new AjaxAdminController();
		$data       = $controller->getDeliveryAddress();

		Response::sendData( $data );
	}

	/**
	 * Returns if the user placed an order this week.
	 */
	public static function getIfUserPlacedOrder(): void {
		static::checkNonce();
		static::checkWooCommerceExist();

		$controller = new AjaxAdminController();
		$data       = $controller->getIfUserPlacedOrder();

		Response::sendData( ['placed_order' => $data] );
	}

	/**
	 * Get user's list of orders for Order History page
	 */
	public function getOrdersList(): void {
		static::checkNonce();
		static::checkWooCommerceExist();

		$controller = new AjaxAdminController();
		$orders_list      = $controller->getOrdersList();

		Response::sendData($orders_list);
	}

	/**
	 * Returns user's orders, containing  a list of recipes with ranking
	 */
	public function getOrderListRecipes() {
		static::checkNonce();
		static::checkWooCommerceExist();

		$controller = new AjaxAdminController();
		$orders_recipes_list      = $controller->getOrdersListRecipes();

		Response::sendData($orders_recipes_list);
	}

	/**
	 * Get details for Order Details page
	 */
	public function getOrder(): void {
		static::checkNonce();
		static::checkWooCommerceExist();
		$errors = new ErrorStore();

		$controller = new AjaxAdminController();
			$order_details = $controller->getOrder($_POST['order_id']);
		Response::sendData(array($order_details));
	}

	/**
	 * Get user's list of taste preference tags
	 */
	public static function getDietaryPreferences(): void {
		static::checkNonce();
		static::checkWooCommerceExist();

		$controller = new AjaxAdminController;
		$result = $controller->getDietaryPreferences();

		Response::sendData($result);
	}

	/**
	 * Update user's list of taste preference tags
	 */
	public static function updateDietaryPreferences(): void {
		static::checkNonce();
		static::checkWooCommerceExist();

		$controller = new AjaxAdminController;
		$include    = $_POST['include'];
		$exclude    = $_POST['exclude'];
		$result     = $controller->updateDietaryPreferences( $include, $exclude );

		Response::sendData( $result );
	}

	/**
	 *  Update delivery address.
	 */
	public static function updateDeliveryAddress(): void {
		static::checkNonce();
		static::checkWooCommerceExist();

		$keys = [
			'first_name',
			'last_name',
			'company',
			'address_1',
			'address_2',
			'city',
			'postcode',
			'country',
			'state',
			'suburb',
			'phone',
			'delivery_instructions'
		];
		$keys = array_flip( $keys );
		$shipping_address = array_intersect_key( $_POST, $keys );

		$controller = new AjaxAdminController();
		$result=$controller->updateDeliveryAddress( $shipping_address );

		Response::sendData( array() );
	}

	/**
	 *  Get user's payment info.
	 */
	public static function getPaymentInfo(): void {
		static::checkNonce();
		static::checkWooCommerceExist();

		$controller = new AjaxAdminController();
		try {
			$data = $controller->getPaymentInfo();
			Response::sendData( $data );
		} catch ( FrontApiException $e ) {
			Response::sendError( $e->getMessage(), $e->getCode() );
		}
	}

	/**
	 * Returns text notification reminders and ondemand ordrer notifications
	 */
	public static function getNotificationStatus(): void {
		static::checkNonce();
		static::checkWooCommerceExist();

		$controller = new AjaxAdminController();
		$notification_status = $controller->getNotificationStatus();

		Response::sendData($notification_status);
	}

	/**
	 *  	 Creates new payment card by payment menthod nonce
	 */
	public static function addCreditCard() {
		static::checkNonce();
		static::checkWooCommerceExist();

		$controller = new AjaxAdminController();

		$paymentmethodnonce = $_POST['paymentmethodnonce'];
		$make_default = $_POST['make_default'];
		try {
			$result = $controller->addCreditCard( $paymentmethodnonce, $make_default );
			Response::sendData( array( $result ) );
		} catch ( \Exception $e ){
			Response::sendError( $e->getMessage(), $e->getCode() );
		}
	}

	/**
	 * take Subject and Description fields from Need Help form and sends email to contact@youplate.it
	 */
	public static function needHelp() {
		static::checkNonce();
		static::checkWooCommerceExist();

		$user_subject = $_POST['user_subject'];
		$description = $_POST['description'];
		$order_id = $_POST['order_id'];

		$controller = new AjaxAdminController();
		$result     = $controller->needHelp( $user_subject, $description, $order_id );

		Response::sendData( array( $result ) );
	}

	/**
	 * Get user ratings for the specify products in order
	 */
	public static function getRatings(){
		static::checkNonce();
		static::checkWooCommerceExist();

		$item_id = $_POST['item_id'];

		$controller = new AjaxAdminController();
		$result = $controller->getRating($item_id);

		Response::sendData($result);
	}

	/**
	 * Update user ratings for the specify products in order
	 */
	public static function updateRatings(){
		static::checkNonce();
		static::checkWooCommerceExist();

		$item_id = $_POST['item_id'];
		$ratings['taste'] = $_POST['taste'];
		$ratings['health'] = $_POST['health'];
		$ratings['cooking_time'] = $_POST['cooking_time'];

		$controller = new AjaxAdminController();
		$result = $controller->updateRating($item_id,$ratings);

		Response::sendData($result);
	}

	/**
	 *  Update account information
	 */
	public static function updateAccountInformation(): void {
		static::checkNonce();
		static::checkWooCommerceExist();

		$keys = [
			'first_name',
			'last_name',
			'user_email',
			'user_pass'
		];

		$keys = array_flip( $keys );

		$account_information = array_intersect_key( $_POST, $keys );

		$controller = new AjaxAdminController();
		$result=$controller->updateAccountInformation( $account_information );

		Response::sendData( array($result) );
	}


	/**
	 * Check nonce code.
	 * If check not passed, echo response with error.
	 */
	protected static function checkNonce(): void {

		if ( ! wp_verify_nonce( $_POST['nonce_code'] ?? '' ) ) {
			$errors = new ErrorStore();
			// Nonce checking was failed.
			Response::sendError( $errors->getErrorMessage( 501 ), 501 );
		}
	}

	/**
	 * Check WooCommerce availability.
	 * If check not passed, echo response with error.
	 */
	protected static function checkWooCommerceExist(): void {

		if ( ! class_exists( 'WooCommerce' ) ) {
			$errors = new ErrorStore();
			// WooCommerce plugin not exist.
			Response::sendError( $errors->getErrorMessage( 401 ), 401 );
		}
	}

	/**
	 * Check WooCommerce Subscriptions availability.
	 * If check not passed, echo response with error.
	 */
	protected static function checkWCSubscriptionExist(): void {

		if ( ! class_exists( 'WC_Subscriptions' ) ) {
			$errors = new ErrorStore();
			// WooCommerce Subscriptions plugin not exist.
			Response::sendError( $errors->getErrorMessage( 403 ), 403 );
		}
	}

	/**
	 * Check WooCommerce composite products availability.
	 * If check not passed, echo response with error.
	 */
	protected static function checkWCCompositeProducts(): void {

		if ( ! class_exists( 'WC_Composite_Products' ) ) {
			$errors = new ErrorStore();
			// WooCommerce composite products plugin not exist.
			Response::sendError( $errors->getErrorMessage( 402 ), 402 );
		}
	}

	/**
	 * Generate nonce code.
	 * Temporary method.
	 * 
	 */
	public static function getNonce(): void {

		if ( ! is_production_environment() ) {
			echo wp_create_nonce();
		}

		wp_die();
	}
}