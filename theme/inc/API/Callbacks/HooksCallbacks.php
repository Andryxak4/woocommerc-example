<?php

namespace FrontApi\Inc\Api\Callbacks;

use FrontApi\Inc\Models\CompositeProductData;
use WC_Cart;
use YPI_Product;
use WC_Subscriptions_Synchroniser;

class HooksCallbacks {
	/**
	 * Update price of composite products in the cart.
	 *
	 * @param WC_Cart $cart       WooCommerce cart instance.
	 */
	public function updateCompositeItemData( WC_Cart $cart): void {
		$total_price = 0;
		$user_id = get_current_user_id();

		/**
		 * Filter cart items.
		 *
		 * Extras: update relative prices for beta users
		 * Composite products: calculate prices
		 * Subscriptions: maybe set free trial
		 * Set price 0 when subscription is made especially on Thursday
		 * (set trial_length = 1 if today is renewal day)
		*/
		$contents = $cart->get_cart_contents();
		$subscription_item = NULL;
		$is_beta = is_beta_user($user_id);
		foreach ( $contents as $key => $item ) {
			if (!is_array($item)) {
				continue;
			}
			switch ( true ) {
				case isset( $item['composite_parent'] ):
					continue 2;
				case isset( $item['composite_children'] ):
					$composite_product_price = $this->calculate_composite_product_price( $item );
					$total_price += $composite_product_price;
					if ($composite_product_price > 0) {
						// Still show this price but do apply discount so we don't bill this now
						WC()->cart->cart_contents[ $item['key'] ]['discount'] = $composite_product_price;
					}
					break;
				case YPI_Product::is_subscription($item['data']->get_id() ):
					// (set trial_length = 1 if today is renewal day)
					$date = WC_Subscriptions_Synchroniser::calculate_first_payment_date( $item['data'], 'timestamp' );
					if (
						WC_Subscriptions_Synchroniser::is_product_synced( $item['data'] )
						&& ! WC_Subscriptions_Synchroniser::is_product_prorated( $item['data'] )
						&& WC_Subscriptions_Synchroniser::is_today( $date )
					) {
						wcs_set_objects_property( $item['data'], 'subscription_trial_length', 1, 'set_prop_only' );
					}
					$subscription_item = $item;
					break;
				case ($is_beta && YPI_Product::is_extra($item['data']->get_id() )):
					// Update prices on extras (beta users only)
					if (isset($item['data']) && isset($item['key'])) {
						$price = (float) $item['data']->get_price();
						if ($price === 0.0) {
							continue 2;
						}
						// Still show this price but do apply discount so we don't bill this now
						WC()->cart->cart_contents[ $key ]['discount'] = $price;
					}
					break;
			}
		}

		// Subscriptions: update price for beta users
		if (!is_null($subscription_item) && $is_beta && $total_price) {
			$subscription_key   = $subscription_item['key'];
			if (!empty($subscription_item['data'])) {
				$subscription_price = (float) $subscription_item['data']->get_regular_price();

				$new_price = (float) ($subscription_price + $total_price);
				if (isset($_GET['action']) && $_GET['action'] === 'get_cart_products') {
					$cart->cart_contents[ $subscription_key ]['base_price'] = $subscription_price;
					$cart->cart_contents[ $subscription_key ]['relative_price'] = $subscription_price;
					$cart->cart_contents[ $subscription_key ]['recurring_price'] = $subscription_price;
					$cart->cart_contents[ $subscription_key ]['data']->set_price($subscription_price);
				} else {
					$cart->cart_contents[ $subscription_key ]['base_price'] = $new_price;
					$cart->cart_contents[ $subscription_key ]['relative_price'] = $new_price;
					$cart->cart_contents[ $subscription_key ]['recurring_price'] = $new_price;
					$cart->cart_contents[ $subscription_key ]['data']->set_price($new_price);
				}
			}
		}
	}

	/**
	 * Calculate and update renewal price of composite product.
	 *
	 * @param array<string, mixed>      $parent          Composite product cart item.
	 * @param array<\WC_Product>|null $cart_items      Children of composite products in the cart.
	 *
	 * @return float
	 *
	 * @throws \FrontApi\Inc\Exceptions\FrontApiException
	 */
	protected function calculate_composite_product_price( array $parent, array $cart_items = null ): float {
		$composite_product_price = 0;
		$children = array();
		$cp_data  = CompositeProductData::getInstance( $parent['product_id'] );
		if (!$cp_data instanceof CompositeProductData) {
			return $composite_product_price;
		}

		if ( ! $cart_items ) {
			$cart_items = WC()->cart->get_cart_contents();
		}

		foreach ( $parent['composite_children'] as $child_key ) {

			if ( ! isset( $cart_items[ $child_key ] ) ) {
				continue;
			}

			$children[] = $cart_items[ $child_key ];
		}


		/**
		 * Servings.
		 */
		$servings_cart_item = $this->get_component_from_cart( 'servings', $children );
		if (!isset($servings_cart_item['product_id'])) {
			// Failed type check - something is wrong here. Abort.
			return $composite_product_price;
		}
		$servings_id             = $servings_cart_item['product_id'];
		$servings_key            = $servings_cart_item['key'];
		$servings                = $cp_data->getServings();
		$price                   = $servings['items'][ $servings_id ]['price'];
		$composite_product_price += $price;
		WC()->cart->cart_contents[ $servings_key ]['relative_price'] = $price;
		//		$servings_cart_item['data']->set_price( $price );

		/*
		 * Removing and adding filter "woocommerce_product_get_price" is needed because in this filter(rather its part in
		 * Woocommerce Subscription plugin) protein prices are resets to zero in every even WC_Cart->calcutate_total method
		 * call. And since the number of these calls is always a multiple of two — in final protein prices always equal to
		 * zero(in calculating the cost of a subscription).
		 */
		remove_filter( 'woocommerce_product_get_price', 'WC_Subscriptions_Cart::set_subscription_prices_for_calculation', 100 );
		/**
		 * Protein.
		 */
		$protein_cart_item                                          = $this->get_component_from_cart( 'protein', $children );
		if (!empty($protein_cart_item['data'])) {
			$protein_id                                                 = $protein_cart_item['product_id'];
			$protein_key                                                = $protein_cart_item['key'];
			$protein                                                    = $cp_data->getProtein();
			$quan                                                       = $servings['items'][ $servings_id ]['people_quantity'];
			$price                                                      = $protein['items'][ $protein_id ]['price'] * ( $quan / 2 );
			$composite_product_price                                    += $price;
			WC()->cart->cart_contents[ $protein_key ]['relative_price'] = $price;
			$protein_cart_item['data']->set_price( (float) $price );
		}
		add_filter( 'woocommerce_product_get_price', 'WC_Subscriptions_Cart::set_subscription_prices_for_calculation', 100, 2 );

		/**
		 * Additional meals.
		 */
		$additional_meal     = $this->get_component_from_cart( 'additional_meal', $children );
		$additional_meal_key = $additional_meal['key'];
		$am_data             = $cp_data->getAdditionalMeal();

		foreach ( $am_data['prices'] as $data ) {
			if ( $data['servings_id'] !== $servings_id ) {
				continue;
			}

			$price                   = $data['price'] * ( $additional_meal['quantity'] - 1 );
			$composite_product_price += $price;
			WC()->cart->cart_contents[ $additional_meal_key ]['relative_price'] = $price;
			//			$additional_meal['data']->set_price( 0 );
			break;
		}

		/**
		 * Extra.
		 */
		$extra_cart_items = $this->get_component_from_cart( 'extra', $children );

		foreach ( $extra_cart_items as $index=>$extra_cart_item ) {
			$extra_id                = $extra_cart_item['product_id'];
			$extra_key               = $extra_cart_item['key'];
			$extra                   = $cp_data->getExtra();
			// @phpstan-ignore-next-line
			$price                   = $extra[$index]['items'][ $extra_id ]['price'];
			$composite_product_price += $price;
			WC()->cart->cart_contents[ $extra_key ]['relative_price'] = $price;
		}

		WC()->cart->cart_contents[ $parent['key'] ]['relative_price'] = $composite_product_price;

		return $composite_product_price;
	}

	/**
	 * Get components form children of composite product.
	 *
	 * @param string $type        Type of composite product component. The type can be:
	 *                            - protein;
	 *                            - servings;
	 *                            - additional_meal;
	 *                            - extra.
	 * @param array<int, array<string, mixed>>  $children    Composite product children.
	 *
	 * @return array<string, mixed>|array<int, array<string, mixed>>
	 * Component cart item or array of cart items (if choose extra type).
	 */
	protected function get_component_from_cart( string $type, array $children ): array {
		$components_list = array();

		foreach ( $children as $component ) {
			$number         = $component['composite_item'];
			$component_type = $component['composite_data'][ $number ]['component_type'];

			if ( $component_type === $type && 'extra' === $type ) {
				$components_list[] = $component;
			} elseif ( $component_type === $type ) {
				return $component;
			}
		}

		return $components_list;
	}
}