<?php

add_action('after_setup_theme', function() {
    add_theme_support('woocommerce');
    add_theme_support('widgets');
    add_theme_support('title-tag');
});

update_option('shop_per_page', 25);

// Alter WooCommerce shop posts per page
function wpex_woo_posts_per_page($cols) {
    return get_option('shop_per_page');
}

add_filter('loop_shop_per_page', 'wpex_woo_posts_per_page');

// add widgets
include_once get_template_directory() . '/inc/widgets.php';
// register sidebars
include_once get_template_directory() . '/inc/sidebars.php';
// add shortdode
include_once get_template_directory() . '/inc/shortcode.php';
// theme woocommerce functions
include_once get_template_directory() . '/inc/woocommerce/wc-template-functions.php';
// theme ajax
include_once get_template_directory() . '/inc/ajax_functions.php';
// add woocommerce actions
require_once get_stylesheet_directory() . '/inc/woocommerce/actions/actions_list.php';

// LionThemes Helper
if (file_exists(get_template_directory() . '/inc/custom-fields.php')) {
    require_once( get_template_directory() . '/inc/custom-fields.php' );
}
if (file_exists(get_template_directory() . '/inc/widgets.php')) {
    require_once( get_template_directory() . '/inc/widgets.php' );
}
if (class_exists('Vc_Manager') && file_exists(get_template_directory() . '/inc/composer-shortcodes.php')) {
    require_once( get_template_directory() . '/inc/composer-shortcodes.php' );
}
if (file_exists(get_template_directory() . '/inc/shortcodes/featurecontent.php')) {
    require_once get_template_directory() . '/inc/shortcodes/featurecontent.php';
}

function hausdampfes_script() {
    global $wp_query;

    wp_enqueue_style('jquery-ui', get_template_directory_uri() . '/assets/css/jquery-ui.min.css');
    wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css');
    wp_enqueue_style('custom', get_template_directory_uri() . '/assets/css/custom.css');

    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery.3.4.1.min.js', [], false, true);
    wp_enqueue_script('jquery-ui', get_template_directory_uri() . '/assets/js/jquery-ui.min.js', ['jquery'], false, true);
    wp_enqueue_script('hausdampfes_libs', get_template_directory_uri() . '/assets/js/libs.min.js', ['jquery'], false, true);
    wp_enqueue_script('smooth-scrollbar', get_template_directory_uri() . '/assets/js/smooth-scrollbar.js', ['jquery'], false, true);
    wp_enqueue_script('wishlist', get_template_directory_uri() . '/assets/js/wishlist-scripts.js', ['jquery', 'hausdampfes_libs'], false, true);
    wp_enqueue_script('hausdampfes_main', get_template_directory_uri() . '/assets/js/main.js', ['jquery', 'hausdampfes_libs'], false, true);

    wp_localize_script('hausdampfes_main', 'hausdampfes', [
        'ajaxurl' => admin_url('admin-ajax.php'),
        'archive_load_more' => [
            'posts' => json_encode($wp_query->query_vars),
            'current_page' => get_query_var('paged') ? get_query_var('paged') : 1,
            'max_page' => $wp_query->max_num_pages,
        ]
    ]);

    //add to cart ajax
    wp_enqueue_script('woo-ajax-add-to-cart', get_template_directory_uri() . '/assets/js/woo-ajax-add-to-cart.js', array('jquery', 'wc-add-to-cart'), false, true);

    if (function_exists('is_product') && is_product()) {
        wp_enqueue_script('woo-ajax-add-to-cart');
    }
}

add_action('wp_enqueue_scripts', 'hausdampfes_script');

add_action('admin_print_styles', 'custom_admin_css');
add_action('wp_enqueue_scripts', 'custom_admin_css');

function custom_admin_css() {
    if (is_admin()) {
        wp_enqueue_style("custom-admin-css", get_template_directory_uri() . '/assets/css/custom_admin.css', false, false, "all");
    }
}

function hausdampfes_admin_enqueue_scripts($hook) {
    // Only add to the nav-menus.php admin page.
    // See WP docs.
    if ('nav-menus.php' !== $hook) {
        return;
    }
    wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery.3.4.1.min.js', [], false, true);
    wp_enqueue_script('custom-admin', get_template_directory_uri() . '/assets/js/custom-admin.js', ['jquery'], false, true);
}

add_action('admin_enqueue_scripts', 'hausdampfes_admin_enqueue_scripts');

register_nav_menus([
    'menutop' => 'Menu Top',
    'menutop_right' => 'Menu Top Right',
    'menu_mobile' => 'Menu Mobile',
]);

function change_submenu_class($classes, $args, $depth) {
    return ['submenu'];
}

add_filter('nav_menu_submenu_css_class', 'change_submenu_class', 10, 3);

function upload_allow_types($mimes) {
    $mimes['webp'] = 'image/webp'; // Adding .webp extension
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'upload_allow_types', 1, 100);

function filter_function_name($data, $file, $filename, $mimes) {

    $filetype = wp_check_filetype($filename, $mimes);

    return [
        'ext' => $filetype['ext'],
        'type' => $filetype['type'],
        'proper_filename' => $data['proper_filename']
    ];
}

add_filter('wp_check_filetype_and_ext', 'filter_function_name', 10, 4);

add_theme_support('post-thumbnails');

// logo
add_image_size('logo_big', 292, 286, true);

// single product slider
add_image_size('hausdampfes_slider', 570, 524, true);
// Blog
add_image_size('blog_post', 1110, 587, true);
add_image_size('blog_archive', 450, 265, true);
// Images of single product tabs.
add_image_size('product_tab_1', 350, 350, true);
add_image_size('product_tab_2', 540, 325, true);

// Order item thumbnail
add_image_size('order_item_thumbnail', 147, 118, true);
// shops
add_image_size('shop_logo', 154, 151, true);



// ================ BEGIN Contact Form 7 =========================
/** Removing tag <p> */
add_filter('wpcf7_autop_or_not', '__return_false');
// ================ END Contact Form 7 ===========================
// ================ BEGIN Cart ===========================
add_filter('woocommerce_cart_totals_coupon_label', function ($coupon, $echo = true) {
    $label = esc_html__('Rabatt', 'hausdampfes');

    if ($echo) {
        echo $label; // WPCS: XSS ok.
    } else {
        return $label;
    }
}, 10, 2);

add_filter('woocommerce_shipping_package_name', function($sprintf, $i, $package) {
    return esc_html__('Versand', 'hausdampfes');
}, 10, 3);

function hausdampfes_wc_cart_totals_shipping_method_label($method, $count_available_methods = 1) {
    $label = '';
    $semicolon = '';
    if ($count_available_methods > 1) {
        $label = $method->get_label();
        $semicolon = ': ';
    }

    $has_cost = 0 < $method->cost;
    $hide_cost = !$has_cost && in_array($method->get_method_id(), array('free_shipping', 'local_pickup'), true);

    if ($has_cost && !$hide_cost) {
        if (WC()->cart->display_prices_including_tax()) {
            $label .= $semicolon . wc_price($method->cost + $method->get_shipping_tax());
            if ($method->get_shipping_tax() > 0 && !wc_prices_include_tax()) {
                $label .= ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>';
            }
        } else {
            $label .= $semicolon . wc_price($method->cost);
            if ($method->get_shipping_tax() > 0 && wc_prices_include_tax()) {
                $label .= ' <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
            }
        }
    }

    return $label;
}

function hausdampfes_get_free_shipping_minimum($zone_name = 'Deutschland') {
    if (!isset($zone_name))
        return null;

    $result = null;
    $zone = null;

    $zones = WC_Shipping_Zones::get_zones();
    foreach ($zones as $z) {
        if ($z['zone_name'] == $zone_name) {
            $zone = $z;
        }
    }

    if ($zone) {
        $shipping_methods_nl = $zone['shipping_methods'];
        $free_shipping_method = null;
        foreach ($shipping_methods_nl as $method) {
            if ($method->id == 'free_shipping') {
                $free_shipping_method = $method;
                break;
            }
        }

        if ($free_shipping_method) {
            $result = $free_shipping_method->min_amount;
        }
    }

    return number_format($result, wc_get_price_decimals(), wc_get_price_decimal_separator(), wc_get_price_thousand_separator());
}

/**
 * Is there a free shipping method in the available shipping methods.
 */
function hausdampfes_get_free_shipping_from_available($available_shipping_methods) {
    $free_shipping_method = false;

    foreach ($available_shipping_methods as $method) {
        if ($method->method_id == 'free_shipping') {
            $free_shipping_method = $method;
            break;
        }
    }
    return $free_shipping_method;
}

function hausdampfes_get_flat_rate_shipping_price($zone_name = 'Deutschland') {
    if (!isset($zone_name))
        return null;

    $result = null;
    $zone = null;

    $zones = WC_Shipping_Zones::get_zones();
    foreach ($zones as $z) {
        if ($z['zone_name'] == $zone_name) {
            $zone = $z;
        }
    }

    if ($zone) {
        $shipping_methods_nl = $zone['shipping_methods'];

        $flat_rate_method = null;
        foreach ($shipping_methods_nl as $method) {
            if ($method->id == 'flat_rate') {
                $flat_rate_method = $method;
                break;
            }
        }

        if ($flat_rate_method) {
            $result = $flat_rate_method->cost;
        }
    }

    return $result;
}

add_action('init', 'hausdampfes_clear_cart_url');

function hausdampfes_clear_cart_url() {
    if (isset($_GET['clear-cart'])) {
        global $woocommerce;
        $woocommerce->cart->empty_cart();
    }
}

// ================ END Cart =============================
// ================ BEGIN Checkout =======================
remove_action('woocommerce_checkout_order_review', 'woocommerce_order_review', 10);
remove_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 10);
add_action('woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20);
add_action('woocommerce_checkout_order_review', 'woocommerce_order_review', 30);

add_filter('woocommerce_checkout_fields', function ($fields) {
    $fields['billing']['billing_birthday'] = [
        'label' => __('Geburtsdatum', 'woocommerce'),
        'type' => 'text',
        'required' => true,
        'id' => 'reg_birthday',
        'class' => [],
        'input_class' => ['form-field', 'js-datepicker'],
        'label_class' => ['form-field-label'],
        'priority' => 1000,
    ];

    return $fields;
}, 10, 1);

add_filter('woocommerce_checkout_posted_data', function ($data) {
    $user_full_name = trim($data['billing_name']);

    $first_name = $user_full_name;
    $last_name = $user_full_name;

    if (strpos($user_full_name, ' ')) {
        $name = explode(' ', $user_full_name);
        $first_name = $name[0];
        $last_name = $name[1];
    }

    $data['billing_first_name'] = $first_name;
    $data['billing_last_name'] = $last_name;

    return $data;
}, 10, 1);

add_action('woocommerce_checkout_update_customer', function (WC_Customer $customer, $data ) {
    if (!empty($data['billing_birthday'])) {
        $customer->update_meta_data('birthday', $data['billing_birthday']);
    }
}, 10, 2);

add_filter('default_checkout_billing_birthday', function ($value, $input) {
    if (empty($value)) {
        $birthdays = get_user_meta(get_current_user_id(), 'birthday');
        try {
            $birthday = (new DateTime($birthdays[0]))->format('d.m.Y');
        } catch (Exception $ex) {
            $birthday = false;
        }

        $value = $birthday;
    }
    return $value;
}, 11, 2);

add_action('woocommerce_checkout_create_order', function (WC_Order $order, $data) {
    if (!empty($data['billing_name'])) {
        $user_full_name = trim($data['billing_name']);

        $first_name = $user_full_name;
        $last_name = $user_full_name;

        if (strpos($user_full_name, ' ')) {
            $name = explode(' ', $user_full_name);
            $first_name = $name[0];
            $last_name = $name[1];
        }

        $order->{"set_billing_first_name"}($first_name);
        $order->{"set_billing_last_name"}($last_name);
    }

    if (!empty($data['shipping_name'])) {
        $user_full_name = trim($data['shipping_name']);

        $first_name = $user_full_name;
        $last_name = $user_full_name;

        if (strpos($user_full_name, ' ')) {
            $name = explode(' ', $user_full_name);
            $first_name = $name[0];
            $last_name = $name[1];
        }

        $order->{"set_shipping_first_name"}($first_name);
        $order->{"set_shipping_last_name"}($last_name);
    }
}, 10, 2);

add_filter('woocommerce_admin_billing_fields', function ( $fields ) {
    $order = wc_get_order(get_the_ID());
    $billing_birthday = $order->get_meta('_billing_birthday');

    $fields['billing_birthday'] = array(
        'label' => __('Birthday', 'hausdampfes'),
        'show' => false,
        'value' => $billing_birthday
    );

    unset($fields['company']);

    return $fields;
}, 10, 1);

add_filter('woocommerce_admin_shipping_fields', function ( $fields ) {
    unset($fields['company']);

    return $fields;
}, 10, 1);

/**
 * Get shipping methods for checkout.
 */
function hausdampfes_wc_cart_totals_shipping_html() {
    $packages = WC()->shipping()->get_packages();
    $first = true;

    foreach ($packages as $i => $package) {
        $chosen_method = isset(WC()->session->chosen_shipping_methods[$i]) ? WC()->session->chosen_shipping_methods[$i] : '';
        $product_names = array();

        if (count($packages) > 1) {
            foreach ($package['contents'] as $item_id => $values) {
                $product_names[$item_id] = $values['data']->get_name() . ' &times;' . $values['quantity'];
            }
            $product_names = apply_filters('woocommerce_shipping_package_details_array', $product_names, $package);
        }

        wc_get_template(
                'checkout/checkout-shipping.php', array(
            'package' => $package,
            'available_methods' => $package['rates'],
            'show_package_details' => count($packages) > 1,
            'show_shipping_calculator' => is_cart() && apply_filters('woocommerce_shipping_show_shipping_calculator', $first, $i, $package),
            'package_details' => implode(', ', $product_names),
            /* translators: %d: shipping package number */
            'package_name' => apply_filters('woocommerce_shipping_package_name', ( ( $i + 1 ) > 1 ) ? sprintf(_x('Shipping %d', 'shipping packages', 'woocommerce'), ( $i + 1)) : _x('Shipping', 'shipping packages', 'woocommerce'), $i, $package),
            'index' => $i,
            'chosen_method' => $chosen_method,
            'formatted_destination' => WC()->countries->get_formatted_address($package['destination'], ', '),
            'has_calculated_shipping' => WC()->customer->has_calculated_shipping(),
                )
        );

        $first = false;
    }
}

add_action('woocommerce_review_order_after_order_total', 'woocommerce_gzd_template_render_checkout_checkboxes', 10);

add_action('init', 'hausdampfes_remove_hooks', 11);

function hausdampfes_remove_hooks() {
    remove_action('woocommerce_checkout_order_review', 'woocommerce_gzd_template_order_submit', has_action('woocommerce_checkout_order_review', 'woocommerce_gzd_template_order_submit'));

    remove_action('woocommerce_review_order_after_order_total', 'woocommerce_gzd_template_cart_total_tax', has_action('woocommerce_review_order_after_order_total', 'woocommerce_gzd_template_cart_total_tax'));

    remove_action('woocommerce_review_order_before_cart_contents', 'woocommerce_gzd_template_checkout_table_content_replacement', has_action('woocommerce_review_order_before_cart_contents', 'woocommerce_gzd_template_checkout_table_content_replacement'));

    remove_action('woocommerce_review_order_after_cart_contents', 'woocommerce_gzd_template_checkout_table_product_hide_filter_removal', has_action('woocommerce_review_order_after_cart_contents', 'woocommerce_gzd_template_checkout_table_product_hide_filter_removal'));

    remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', has_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description'));
}

if (function_exists('wc_gzd_get_hook_priority')) {
    add_action('woocommerce_review_order_after_order_total', 'woocommerce_gzd_template_order_submit', wc_gzd_get_hook_priority('checkout_order_submit'));
}

if (!function_exists('hausdampfes_woocommerce_taxonomy_archive_description')) {

    /**
     * Show an archive description on taxonomy archives.
     */
    function hausdampfes_woocommerce_taxonomy_archive_description() {
        if (is_product_taxonomy() && 0 === absint(get_query_var('paged'))) {
            $term = get_queried_object();

            if ($term && !empty($term->description)) {
                echo '<div class="term-description container" style="padding-top: 15px; padding-bottom: 15px">' . wc_format_content($term->description) . '</div>'; // WPCS: XSS ok.
            }
        }
    }

    /**
     * Archive descriptions.
     *
     * @see woocommerce_taxonomy_archive_description()
     */
    add_action('woocommerce_archive_description', 'hausdampfes_woocommerce_taxonomy_archive_description', 10);
}

/**
 * Gets and formats a list of cart item data + variations for display on the frontend.
 *
 * @since 3.3.0
 * @param array $cart_item Cart item object.
 * @param bool  $flat Should the data be returned flat or in a list.
 * @return string
 */
function hausdampfes_wc_get_formatted_cart_item_data_for_checkout($cart_item, $flat = false) {
    $item_data = array();

    // Variation values are shown only if they are not found in the title as of 3.0.
    // This is because variation titles display the attributes.
    if ($cart_item['data']->is_type('variation') && is_array($cart_item['variation'])) {
        foreach ($cart_item['variation'] as $name => $value) {
            $taxonomy = wc_attribute_taxonomy_name(str_replace('attribute_pa_', '', urldecode($name)));

            if (taxonomy_exists($taxonomy)) {
                // If this is a term slug, get the term's nice name.
                $term = get_term_by('slug', $value, $taxonomy);
                if (!is_wp_error($term) && $term && $term->name) {
                    $value = $term->name;
                }
                $label = wc_attribute_label($taxonomy);
            } else {
                // If this is a custom option slug, get the options name.
                $value = apply_filters('woocommerce_variation_option_name', $value, null, $taxonomy, $cart_item['data']);
                $label = wc_attribute_label(str_replace('attribute_', '', $name), $cart_item['data']);
            }

            // Check the nicename against the title.
            if ('' === $value || wc_is_attribute_in_product_name($value, $cart_item['data']->get_name())) {
                continue;
            }

            $item_data[] = array(
                'key' => $label,
                'value' => $value,
            );
        }
    }

    // Filter item data to allow 3rd parties to add more to the array.
    $item_data = apply_filters('woocommerce_get_item_data', $item_data, $cart_item);

    // Format item data ready to display.
    foreach ($item_data as $key => $data) {
        // Set hidden to true to not display meta on cart.
        if (!empty($data['hidden'])) {
            unset($item_data[$key]);
            continue;
        }
        $item_data[$key]['key'] = !empty($data['key']) ? $data['key'] : $data['name'];
        $item_data[$key]['display'] = !empty($data['display']) ? $data['display'] : $data['value'];
    }

    // Output flat or in list format.
    if (count($item_data) > 0) {
        ob_start();

        if ($flat) {
            foreach ($item_data as $data) {
                echo esc_html($data['key']) . ': ' . wp_kses_post($data['display']) . "\n";
            }
        } else {
            wc_get_template('checkout/cart-item-data.php', array('item_data' => $item_data));
        }

        return ob_get_clean();
    }

    return '';
}

// ================ END Checkout =========================
// ================ BEGIN My Account =========================
add_filter('woocommerce_my_account_my_address_formatted_address', function($address, $customer_id, $address_type) {
    $customer = new WC_Customer($customer_id);
    if ('billing' == $address_type) {
        $address['billing_name'] = $customer->get_meta('billing_name');
    } else if ('shipping' == $address_type) {
        $address['shipping_name'] = $customer->get_meta('shipping_name');
    }

    return $address;
}, 999, 3);

add_filter('woocommerce_formatted_address_replacements', function ($placeholder, $args) {

    if (!empty($args['billing_name'])) {
        $placeholder['{name}'] = $args['billing_name'];
    } else if (!empty($args['shipping_name'])) {
        $placeholder['{name}'] = $args['shipping_name'];
    }

    return $placeholder;
}, 999, 2);
// ================ END My Account ===========================
// ================== BEGIN excerpt =====================
add_filter('the_excerpt', function($excerpt) {
    return hausdampfes_make_excerpt($excerpt, 250);
}, 999, 1);

function hausdampfes_product_short_title($title) {
    return hausdampfes_make_excerpt($title, 50);
}

function hausdampfes_make_excerpt($string, $limit, $end = '&hellip;') {
    if (mb_strlen($string) > $limit) {
        return mb_substr($string, 0, $limit) . $end;
    }
    return $string;
}

// ================== END excerpt =======================
// ==================== BEGIN comments ===================
add_filter('get_comment_date', function ($date, $format, $comment ) {
    return date_format(date_create($comment->comment_date), 'j.n.Y');
}, 999, 3);

add_filter('get_comment_time', function ($date, $format, $gmt, $translate, $comment) {
    return date_format(date_create($comment->comment_date), 'H:i');
}, 999, 5);

add_filter('comment_form_defaults', function($fields) {
    $fields['comment_notes_before'] = sprintf(
            '<p class="comment-notes">%s</p>', sprintf(
                    '<span id="email-notes">%s</span>', esc_html__('Deine E-Mail-Adresse wird nicht veröffentlicht.', 'hausdampfes')
            )
    );

    return $fields;
}, 999, 1);

add_filter('comment_form_fields', function($fields) {
    $rearranged = [];

    $rearranged['author'] = !empty($fields['author']) ? $fields['author'] : null;
    $rearranged['email'] = !empty($fields['email']) ? $fields['email'] : null;
    $rearranged['comment'] = !empty($fields['comment']) ? $fields['comment'] : null;

    if (!empty($fields['cookies'])) {
        $commenter = wp_get_current_commenter();
        $consent = empty($commenter['comment_author_email']) ? '' : ' checked="checked"';

        $rearranged['cookies'] = sprintf(
                '<p class="comment-form-cookies-consent-text">%s</p><p class="comment-form-cookies-consent">%s %s</p>', esc_html__('Dieses Formular speichert Ihren Namen, Ihre Email Adresse sowie den Inhalt, damit wir die Kommentare auf unsere Seite auswerten können. Weitere Informationen finden Sie auf unserer Seite der Datenschutzbestimmungen.', 'hausdampfes'), sprintf(
                        '<input id="wp-comment-cookies-consent" name="wp-comment-cookies-consent" type="checkbox" required value="yes"%s />', $consent
                ), sprintf(
                        '<label for="wp-comment-cookies-consent">%s</label><span class="wp-comment-cookies-consent__error">%s</span>', esc_html__('Ich akzeptiere', 'hausdampfes'), esc_html__('Sie müssen unseren Bedingungen zustimmen', 'hausdampfes')
                )
        );
    }

    return $rearranged;
}, 999, 1);
// ==================== END comments =====================
// ========================= BEGIN Filter product with empty price =====================
/**
 * Hide products with empty price
 */
add_action('woocommerce_product_query', function (WP_Query $q) {
    if (is_admin()) {
        return;
    }

    $meta_query = $q->get('meta_query');
    $meta_query[] = array(
        'key' => '_price',
        'value' => 0,
        'compare' => '>'
    );
    $q->set('meta_query', $meta_query);
}, 10, 1);

/**
 * Hide related products with empty price
 */
add_filter('woocommerce_related_products', function($related_posts) {
    $filtered = [];
    foreach ($related_posts as $related_post_id) {
        $product = wc_get_product($related_post_id);
        if ($product->is_type('variable')) {
            $prices = $product->get_variation_prices(true);

            if (!empty($prices['price'])) {
                $filtered[] = $related_post_id;
            }
        } else if ($product->is_type('simple')) {
            if ('' != $product->get_price() && 0 != $product->get_price()) {
                $filtered[] = $related_post_id;
            }
        }
    }

    return $filtered;
}, 99, 1);

// ========================= END Filter product with empty price =======================

function rating_stars($rating) {
    $string = '';
    $rating = round($rating);
    if ($rating != '') {
        for ($i = 0; $i < 5; $i++) {
            if ($rating > 0) {
                $string .= '<li><span class="icon-Star" style="color: #CB9D4A"></span></li>';
                $rating--;
            } else {
                $string .= '<li><span class="icon-Star" style="color: #c4c4c4"></span></li>';
            }
        }
    }
    return $string;
}

function hausdampfes_get_formatted_price($price, $price_args) {
    $price = apply_filters('formatted_woocommerce_price', number_format($price, $price_args['decimals'], $price_args['decimal_separator'], $price_args['thousand_separator']), $price, $price_args['decimals'], $price_args['decimal_separator'], $price_args['thousand_separator']);

    return sprintf($price_args['price_format'], get_woocommerce_currency_symbol($price_args['currency']), $price);
}

add_filter('woocommerce_form_field', function( $field, $key, $args, $value ) {
    if (strpos($field, '</span>') !== false && $args['required']) {
        $error = '<span class="error" style="display:none">';
        $error .= sprintf(__('%s Ist ein Pflichtfeld.', 'woocommerce'), $args['label']);
        $error .= '</span>';
        $field = substr_replace($field, $error, strpos($field, '</span>'), 0);
    }

    return $field;
}, 10, 4);



add_action('woocommerce_order_status_changed', 'update_order_status_to_send_to_flour', 10, 4);

function update_order_status_to_send_to_flour($orderId, $oldStatus, $newStatus, WC_Order $orderObject) {




    if ($newStatus !== 'processing' || $orderObject->get_shipping_country() !== 'DE') {
        return null;
    }

    $orderItems = $orderObject->get_items();

    if (empty($orderItems) || !is_array($orderItems)) {
        return null;
    }

    foreach ($orderItems as $item_id => $orderItem) {

        $variation_id = $orderItem->get_variation_id();
        if (!empty($variation_id)):
            $is_api = empty(get_post_meta($variation_id, '_flourio_article_id', true)) ? false : true;
        else:
            $product_id = $orderItem->get_product_id();
            $is_api = empty(get_post_meta($product_id, '_flourio_article_id', true)) ? false : true;
        endif;

        if (!$is_api) {
            return null;
        }
    }


    $result = sendOrderToFlour($orderId);
}

function sendOrderToFlour($orderId) {
    require_once(ABSPATH . 'flourio_api/flourio.php');
    createdNewOrder($orderId);
}

function iconic_variable_price_format($price, $product) {
    $min_price_regular = $product->get_variation_regular_price('min', true);
    $min_price_sale = $product->get_variation_sale_price('min', true);
    $max_price = $product->get_variation_price('max', true);
    $min_price = $product->get_variation_price('min', true);
    $price = ( $min_price_sale == $min_price_regular ) ?
            wc_price($min_price_regular) :
            '<ins>' . wc_price($min_price_sale) . '</ins>' . $product->get_price_suffix();
    return ( $min_price == $max_price ) ?
            $price :
            sprintf('%s%s', '', $price);
}

add_filter('woocommerce_variable_sale_price_html', 'iconic_variable_price_format', 10, 2);
add_filter('woocommerce_variable_price_html', 'iconic_variable_price_format', 10, 2);

function hd_get_stock_html($product) {
    if ($product->is_type('variable')) {
        $variations = $product->get_available_variations();
        if ($variations) {
            $count = '';
            foreach ($variations as $variation) {
                if ($variation['max_qty'] > $count) {
                    $count = $variation['max_qty'];
                    $choice_variant = $variation;
                }
            }
            if ($choice_variant) {
                $product = wc_get_product($choice_variant['variation_id']);
            }
        }
    }
    $html = '';
    $availability = $product->get_availability();

    if (!empty($availability['availability'])) {
        $stock = $product->get_stock_quantity();
        if ($stock > 5) {
            $color = 'color_green';
        } elseif ($stock > 0 && $stock < 6) {
            $color = 'color_yellow';
        } else {
            $color = 'color_red';
        }
        ob_start();

        wc_get_template(
                'single-product/stock.php', array(
            'product' => $product,
            'class' => $availability['class'] . ' ' . $color,
            'availability' => $availability['availability'],
                )
        );

        $html = ob_get_clean();
    }

    if (has_filter('woocommerce_stock_html')) {
        wc_deprecated_function('The woocommerce_stock_html filter', '', 'woocommerce_get_stock_html');
        $html = apply_filters('woocommerce_stock_html', $html, $availability['availability'], $product);
    }

    return apply_filters('woocommerce_get_stock_html', $html, $product);
}
